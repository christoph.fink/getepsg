#!/usr/bin/python -u
# -*- coding: utf-8 -*-

from __future__ import print_function
from osgeo import osr,ogr
import sys

def main():
	try:
		if len(sys.argv) != 2:
			raise Exception("Please supply exactly one parameter:")
		
		dataset=ogr.Open(sys.argv[1])
		if dataset is None:
			ogr.Open(sys.argv[1]+".shp") # filename supplied without extension? 
		if dataset is None:
			raise Exception("Could not open file %s." %(sys.argv[1]))
		layer=dataset.GetLayerByIndex() # first layer
		srs=layer.GetSpatialRef()
		srs.AutoIdentifyEPSG()
		if srs.GetAttrValue("AUTHORITY",1) is None:
			print("No EPSG equivalent found :/", file=sys.stderr)
			return -2
		print(srs.GetAttrValue("AUTHORITY", 1))
		return 0
	
	except Exception as e:
		print(e)
		print("""Usage: %s [filename]""" %(sys.argv[0]))
		return -1

if __name__ == '__main__':
	exit(main())
